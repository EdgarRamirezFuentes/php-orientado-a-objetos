<?php
    class PasswordGenerator {
        private $password;

        // Genera una contraseña psuedoaleatoria de 4 caracteres
        public function __construct() {
            // Almacena los caracteres que pueden formar la contraseña
            $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            $this->password = $letters[rand(0, 25)] . $letters[rand(0, 25)] . $letters[rand(0, 25)] . $letters[rand(0, 25)];
        }

        // Imprime la contraseña generada
        public function __destruct() {
            echo "<p>La password ha sido destruida</p>";
            $this->password = null;

        }

        // Devuelve la contraseña generada
        public function get_password() {
            return $this->password;
        }
    }

    if (!empty($_POST)){
        //creacion de objeto de la clase
        $pass= new PasswordGenerator();
        $mensaje_password='Este es tu password: '. $pass->get_password();
    }
    
?>