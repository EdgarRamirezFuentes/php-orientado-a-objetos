<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $anio;
	private $resultado;

	// Inicializa los valores de cada objeto
	public function __construct($color = null, $modelo = null, $anio = null) {
		$this->color = $color === null ? 'n/a' : $color;
		$this->modelo = $modelo === null ? 'n/a' : $modelo;
		$this->anio = $anio === null ? 'n/a' : $anio;
		$this->resultado = 'n/a';
	}
	//declaracion del método verificación
	public function verificacion(){
		if ($this->anio < 1900) {
			$this->resultado = 'No';
		} elseif ($this->anio >= 1900 && $this->anio <= 2010) {
			$this->resultado  = 'Revisión';
		} else {
			$this->resultado = 'Sí';
		}
	}

	// Obtiene el valor del resultado de la verificación
	public function get_resultado() {
		return $this->resultado;
	}
}

//creación de instancia a la clase Carro

if (!empty($_POST)){
	// Crea instancia de la clase Carro2 usando el constructor que inicializa los valores de la instancia
	$Carro = new Carro2($_POST['color'], $_POST['modelo'], (int) substr($_POST['anio'], 0, 4));
	// Obtiene el resultado de a verificación
	$Carro->verificacion();
}




