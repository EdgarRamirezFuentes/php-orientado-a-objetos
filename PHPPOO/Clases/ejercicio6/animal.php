<?php 
    include_once("./transporte.php");

    class Animal extends transporte{
        private $tipo;
        public function __construct($nom,$vel,$com,$tipo){
			parent::__construct($nom,$vel,$com);
			$this->tipo=$tipo;
		}

		// sobreescritura de metodo
		public function resumenAnimal(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Tipo de animal:</td>
						<td>'. $this->tipo.'</td>				
					</tr>';
			return $mensaje;
		}
    }

    if ($_POST['tipo_transporte'] === 'animal') {
        $burro = new Animal('Juancho','50','Vegetales','Burro');
        $mensaje=$burro->resumenAnimal();
    }
?>